﻿using Assignment4.Presentation;
using Assignment4.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assignment4
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        User _user;
        Horoscope _horoscope;

        public MainPage()
        {
            this.InitializeComponent();
            _user = new User();
            _horoscope = new Horoscope();

            _frmContent.Navigate(typeof(DailyHoroscopePage), _user);
            _frmContent.Navigate(typeof(PredictionEditorPage), _horoscope);

            SystemNavigationManager navMgr = SystemNavigationManager.GetForCurrentView();
            navMgr.BackRequested += OnNavigateBack;
        }

        private void OnNavigateBack(object sender, BackRequestedEventArgs e)
        {
            if (_frmContent.CanGoBack)
            {
                _frmContent.GoBack();
                e.Handled = true;
            }
        }

        private void OnNavigationItemClick(object sender, ItemClickEventArgs e)
        {


            NavMenuItem clickedMenuItem = e.ClickedItem as NavMenuItem;
            if (clickedMenuItem == _uiNavHome)
            {
                _navSplitView.IsPaneOpen = false;
                _frmContent.Navigate(typeof(DailyHoroscopePage), _user);
            }
            else if (clickedMenuItem == _uiNavProfile)
            {
                //close the pane before navigating to the next page if the split view is in overlay mode. 
                //Otherwise, the Back button will first need to close the pane. When the pane is not visible on
                //the destination page, it looks like the first Back button is ignored.
                if (_navSplitView.DisplayMode == SplitViewDisplayMode.Overlay || _navSplitView.DisplayMode == SplitViewDisplayMode.CompactOverlay)
                {
                    _navSplitView.IsPaneOpen = false;
                }

                _frmContent.Navigate(typeof(PredictionEditorPage));
            }
            else if (clickedMenuItem == _uiNavEditPrediction)
            {
                if (_navSplitView.DisplayMode == SplitViewDisplayMode.Overlay || _navSplitView.DisplayMode == SplitViewDisplayMode.CompactOverlay)
                {
                    _navSplitView.IsPaneOpen = false;
                }

                _frmContent.Navigate(typeof(PredictionEditorPage));

            }
            else
            {
                Debug.Assert(false, "Unknown menu item");
            }


        }

        private void OnContentFrameNavigated(object sender, NavigationEventArgs e)
        {
            _lstAppNavigation.IsEnabled = true;
            if (e.SourcePageType == typeof(DailyHoroscopePage))
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
                _lstAppNavigation.SelectedItem = _uiNavHome;
                _txtPageTitle.Text = "Daily Horoscope";
            }
            else if (e.SourcePageType == typeof(PredictionEditorPage))
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
                _lstAppNavigation.SelectedItem = _uiNavEditPrediction;
                _txtPageTitle.Text = "Prediction Editor";
            }
        }
    }
}
