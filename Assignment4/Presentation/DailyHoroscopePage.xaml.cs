﻿using Assignment4.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Assignment4.Presentation
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DailyHoroscopePage : Page
    {
        private User _user;

        private ZodiacSign _zodiacSign;

//        DateTime aquarius = new DateTime(1, 20);
  //      DateTime pisces = 

        public DailyHoroscopePage()
        {
            this.InitializeComponent();
            _user = new User();
         //   _zodiacSign.CreateSigns();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _user = e.Parameter as User;
        }

        private void OnApply(object sender, RoutedEventArgs e)
        {
            _profileSplitView.IsPaneOpen = false;
            _imgUser.Source = _imgUserSplit.Source;

            string name = _txtNameEnter.Text;
            _user.Name = name;
            DateTime date = _calendarBirthday.Date.Date;
            _user.Birthday = date;

         //   if(date.Date.Date)
            DisplayDetails();
        }

        private void DisplayDetails()
        {
            _txtName.Text = "Name: " + _user.Name;
           _txtBirthday.Text = "Birthday: "+ _user.Birthday;
        }

        private async void OnSelectImage(object sender, RoutedEventArgs e)
        {
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".jpeg");
            openPicker.FileTypeFilter.Add(".png");
            openPicker.FileTypeFilter.Add(".bmp");
            StorageFile file = await openPicker.PickSingleFileAsync();

            //store the avatar image file to application data storage
            StorageFile avatarImageFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("AvatarImage", CreationCollisionOption.ReplaceExisting);
            await file.CopyAndReplaceAsync(avatarImageFile);

            //display the new image 
            LoadImageAvatar();
        }

        private async void LoadImageAvatar()
        {
            try
            {
                //obtain the storage file for the avatar image to use 
                StorageFile avatarImageFile = await ApplicationData.Current.LocalFolder.GetFileAsync("AvatarImage");

                //obtain a stream for reading the image data
                using (IRandomAccessStream imgStream = await avatarImageFile.OpenAsync(FileAccessMode.Read))
                {
                    BitmapImage image = new BitmapImage();
                    image.SetSourceAsync(imgStream);
                    _imgUserSplit.Source = image;
                }
            }
            catch (FileNotFoundException)
            {
                //obtain the file from the app assets and put the file in local storage
                StorageFile avatarImgAssetFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/noImage.png"));

                //copy the file into local storage
                avatarImgAssetFile.CopyAsync(ApplicationData.Current.LocalFolder);

                //display the asset avatar as the image source
                _imgUserSplit.Source = new BitmapImage()
                {
                    UriSource = new Uri("ms-appx:///Assets/noImage.png")
                };
            }
            catch (IOException)
            {
                _imgUserSplit.Source = new BitmapImage()
                {
                    UriSource = new Uri("ms-appx:///Assets/noImage.png")
                };
            }
        }
    }
}
