﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Assignment4.BusinessLogic
{
    class User
    {
        private string _name;

        private DateTime _birthday;

        private Image _avatar;

        public User()
        {
            _name = Name;
            _birthday = Birthday;
            _avatar = Avatar;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public DateTime Birthday
        {
            get { return _birthday; }
            set { _birthday = value; }
        }

        public Image Avatar
        {
            get { return _avatar; }
            set { _avatar = value; }
        }


    }
}
