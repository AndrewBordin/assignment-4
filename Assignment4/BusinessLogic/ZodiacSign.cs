﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Assignment4.BusinessLogic
{
    class ZodiacSign
    {

        private List<Image> _zodiacList;

        private Image Aries;

        public ZodiacSign()
        {
            _zodiacList = ZodiacList;
        }

        public List<Image> ZodiacList
        {
            get { return _zodiacList; }
            set { _zodiacList = value; }
        }

        public void CreateSigns()
        {
            //TODO: Add the images of the signs to the zodiaclist
        }
    }
}
